﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRotate : MonoBehaviour {
    public Transform _target;

    bool _rotating;

    private void Update() {
        if (Input.GetMouseButtonDown(0)) {
            _rotating = true;
        }

        if (_rotating) {
            transform.RotateAround(_target.position, Vector3.up, Input.GetAxis("Mouse X"));
        }

        if (Input.GetMouseButtonUp(0)) {
            _rotating = false;
        }
    }

}