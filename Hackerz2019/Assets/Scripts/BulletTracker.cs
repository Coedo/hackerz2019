﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class BulletTracker : MonoBehaviour {
    public Vector3 _offset;
    public UnityEngine.UI.RawImage renderTarget;
    public UnityEngine.UI.RawImage renderTarget2;

    public TurnSystem turns;

    Transform _target;

    bool _tracking;

    private void Start() {
        var treb = FindObjectsOfType<Trebuchet>();
        foreach (var item in treb) {
            item.OnFired += Track;
        }
        turns.OnEndTurn += EndTurn;
    }

    private void FixedUpdate() {
        if (_target == null) {
            _tracking = false;
        }

        if (_tracking) {
            transform.position = Vector3.Slerp(transform.position, _target.position + _offset, 0.2f);
            transform.LookAt(_target);
        }
    }

    void Track(Bullet b) {
        _target = b.GetTrackingTransform();
        _tracking = true;
        renderTarget.gameObject.SetActive(true);
        
        if(renderTarget2 != null)
            renderTarget2.gameObject.SetActive(true);
    }

    void EndTurn(PlayerController player) {
        renderTarget.gameObject.SetActive(false);
        if(renderTarget2 != null)
            renderTarget2.gameObject.SetActive(false);
    }
}