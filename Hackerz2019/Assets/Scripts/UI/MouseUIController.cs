﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MouseUIController : UIController {
    public BulletsLevelList _BulletsList;
    public AmmoButton _AmmoButtonPrefab;
    public Transform _ButtonsContainer;
    public Trebuchet _Trebuchet;

    List<AmmoButton> _AmmoButtons = new List<AmmoButton>();

    public Slider powerSlider;
    public Slider rotationSlider;

    
    public override void SetTrebuchet(Trebuchet newTrebuchet)
    {
        if (_Trebuchet != null)
        {
            _Trebuchet.OnFired -= OnFire;
        }

        _Trebuchet = newTrebuchet;
        _Trebuchet.OnFired += OnFire;
        RefreshUI();
    }


    public override void SetFirePower(float power)
    {
        _Trebuchet.SetPowerPercentage(power);
    }

    public override void SetRoration(float value)
    {
        _Trebuchet.SetRotationPercentage(value);
    }


    private void Start() {
        if (_Trebuchet != null)
            _Trebuchet.OnFired += OnFire;

        var bullets = _BulletsList.bulletData;
        for (int i = 0; i < bullets.Length; i++) {
            var button = Instantiate(_AmmoButtonPrefab, _ButtonsContainer);
            button.Initialize(bullets[i], i, bullets[i].sprite, this);

            _AmmoButtons.Add(button);
        }

        foreach (var item in _AmmoButtons) {
            item.RefreshAmmoCount(_Trebuchet);
        }
        RefreshUI();

        GameManager.Instance.OnGameEnd += Instance_OnGameEnd;
    }

    private void Instance_OnGameEnd(string winner)
    {
        ShowGameOver(winner);
    }

    private void RefreshUI()
    {
        if (_Trebuchet != null)
        {            
            foreach (var item in _AmmoButtons)
            {
                item.RefreshAmmoCount(_Trebuchet);
            }
            powerSlider.value = _Trebuchet._Power / _Trebuchet._MaxPower;
            rotationSlider.value = (_Trebuchet.transform.eulerAngles.y - _Trebuchet._RotationMinMax.x) / (_Trebuchet._RotationMinMax.y - _Trebuchet._RotationMinMax.x);
        }
        gameObject.SetActive(_Trebuchet != null);
    }

    void OnFire(Bullet b) {
        foreach (var item in _AmmoButtons) {
            item.RefreshAmmoCount(_Trebuchet);
        }
    }

}