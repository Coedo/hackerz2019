﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AmmoButton : MonoBehaviour {

    public Text _AmmoText;
    public Image _Image;

    BulletData _data;
    int _index;

    Trebuchet _currentTreb => _UIController._Trebuchet;
    MouseUIController _UIController;

    public void Initialize(BulletData data, int index, Sprite img, MouseUIController uIController) {
        _data = data;

        _Image.sprite = img;

        _index = index;

        _UIController = uIController;
    }

    public void RefreshAmmoCount(Trebuchet treb) {
        if (treb != null)
        {
            _AmmoText.text = _data.infinity ? "" : treb.GetAmmo(_index).ToString();
        }
    }

    public void SetAmmoIndex() {
        _currentTreb.SetBulletIndex(_index);
    }
}