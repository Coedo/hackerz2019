﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;

public class VRUIController : UIController {

    public BulletsLevelList _BulletsList;
    public List<VRAmmoButton> _AmmoButton;
    public Transform _ButtonsContainer;
    public Trebuchet _Trebuchet;  

    public Slider powerSlider;
    public Slider rotationSlider;

    public LinearMapping linearPowerMapping;
    public LinearMapping linearRotationMapping;

    public LinearDrive linearRotationDrive;
    public CircularDrive circularPowerDrive;

    private void Start()
    {
        if (_Trebuchet != null)
            _Trebuchet.OnFired += OnFire;

        var bullets = _BulletsList.bulletData;
        for (int i = 0; i < _AmmoButton.Count; i++) {
            _AmmoButton[i].Initialize(bullets[i], i, null, this);            
        }

        RefreshUI();
        GameManager.Instance.OnGameEnd += Instance_OnGameEnd;
    }

    private void Instance_OnGameEnd(string winner)
    {
        ShowGameOver(winner);
    }

    private void Update()
    {
        if(powerSlider.value != linearPowerMapping.value)
        {
            powerSlider.value = linearPowerMapping.value;
        }
        if (rotationSlider.value != linearRotationMapping.value)
        {
            rotationSlider.value = linearRotationMapping.value;
        }
    }

    public override void SetTrebuchet(Trebuchet newTrebuchet)
    {
        if(_Trebuchet != null)
        {
            _Trebuchet.OnFired -= OnFire;
        }

        _Trebuchet = newTrebuchet;
        _Trebuchet.OnFired += OnFire;
        RefreshUI();
    }

    

    public void RefreshAmmoButtons()
    {
        if (_Trebuchet != null)
        {
            foreach (var item in _AmmoButton)
            {
                item.RefreshSelectedVRButton();
            }
        }

    }
    private void RefreshUI()
    {
        //gameObject.SetActive(_Trebuchet != null);
        if (_Trebuchet != null)
        {            
            foreach (var item in _AmmoButton)
            {
                item.RefreshAmmoCount(_Trebuchet);
            }
            powerSlider.value = _Trebuchet._Power / _Trebuchet._MaxPower;
            rotationSlider.value = (_Trebuchet.transform.eulerAngles.y - _Trebuchet._RotationMinMax.x) / (_Trebuchet._RotationMinMax.y - _Trebuchet._RotationMinMax.x);

            foreach (var item in _AmmoButton)
            {
                item.RefreshAmmoCount(_Trebuchet);
                item.RefreshSelectedVRButton();
            }
            UpdateVRElements();

        }
    }

    private void UpdateVRElements()
    {
        circularPowerDrive.outAngle = Mathf.Lerp(circularPowerDrive.minAngle, circularPowerDrive.maxAngle,  powerSlider.value);
        circularPowerDrive.transform.localEulerAngles = new Vector3(0, circularPowerDrive.outAngle, 0);
        linearPowerMapping.value = powerSlider.value;


        linearRotationDrive.transform.localPosition = Vector3.Lerp(linearRotationDrive.startPosition.localPosition, linearRotationDrive.endPosition.localPosition, rotationSlider.value);
        linearRotationMapping.value = rotationSlider.value;
    }

    void OnFire(Bullet b) {
        foreach (var item in _AmmoButton) {
            item.RefreshAmmoCount(_Trebuchet);
        }
    }

    public override void SetFirePower(float power) {
        if(_Trebuchet)
        {

        _Trebuchet.SetPowerPercentage(power);
        }
    }

    public override void SetRoration(float value) {
        if (_Trebuchet)
        {
            _Trebuchet.SetRotationPercentage(value);
        }
    }
}