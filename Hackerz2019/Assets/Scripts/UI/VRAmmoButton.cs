﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VRAmmoButton : MonoBehaviour
{
    public Text _AmmoText;

    BulletData _data;
    int _index;

    Trebuchet _currentTreb => _UIController._Trebuchet;
    VRUIController _UIController;

    public MeshRenderer baseRenderer;

    public void Initialize(BulletData data, int index, Sprite img, VRUIController uIController)
    {
        _data = data;

        _index = index;

        _UIController = uIController;
        RefreshSelectedVRButton();
        RefreshAmmoCount(_currentTreb);
    }

    public void RefreshAmmoCount(Trebuchet treb)
    {
        if (treb != null)
        {
            _AmmoText.text = _data.infinity ? "" : treb.GetAmmo(_index).ToString();
        }
    }

    public void SetAmmoIndex()
    {
        _currentTreb.SetBulletIndex(_index);
        RefreshSelectedVRButton();
        RefreshAmmoCount(_currentTreb);
        _UIController.RefreshAmmoButtons();
    }

    public void RefreshSelectedVRButton()
    {
        if (_currentTreb != null)
        {
            if (_currentTreb._CurrentBulletIndex == _index)
            {
                baseRenderer.material.color = Color.red;
            }
            else
            {
                baseRenderer.material.color = Color.white;
            }
        }
    }
}
