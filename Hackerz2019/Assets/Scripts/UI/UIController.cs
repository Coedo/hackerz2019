﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public event Action OnFireClick;

    public GameObject playerTurn;
    public GameObject enemyTurn;
    public Text castleHealth;
    public Text unitsAlive;

    public GameObject waitingForOtherPlayer;

    public GameObject UnitsInfoUI;
    public Text unitsLeftText;

    public GameObject GameOverWindow;
    public Text playerWinText;

    public virtual void SetTrebuchet(Trebuchet newTrebuchet)
    {

    }


    public virtual void SetFirePower(float power)
    {
        
    }

    public virtual void SetRoration(float value)
    {
        
    }

    public void FireButton()
    {
        if (OnFireClick != null)
        {
            OnFireClick();
        }
    }

    public virtual void UpdateUnitsToSet(int value)
    {
        if (UnitsInfoUI != null)
        {
            UnitsInfoUI.SetActive(true);
            unitsLeftText.text = value.ToString();
        }
    }

    public virtual void HideUnitsInfo()
    {
        if (UnitsInfoUI != null)
        {
            UnitsInfoUI.SetActive(false);
        }
    }

    public virtual void ShowWaitingForSecondPlayer()
    {
        if (waitingForOtherPlayer)
        {
            waitingForOtherPlayer.SetActive(true);
        }
    }

    public virtual void HideWaitingForSecondPlayer()
    {
        if (waitingForOtherPlayer)
        {
            waitingForOtherPlayer.SetActive(false);
        }
    }

    public virtual void ShowPlayersTurn()
    {
        if (playerTurn && enemyTurn)
        {
            playerTurn.SetActive(true);
            enemyTurn.SetActive(false);
        }
    }
    public virtual void ShowOtherPlayerTurn()
    {
        if (playerTurn && enemyTurn)
        {
            playerTurn.SetActive(false);
            enemyTurn.SetActive(true);
        }
    }

    public virtual void UpdateCastleHealthText(string castleHealth)
    {
        if (castleHealth != null)
        {
            this.castleHealth.text = castleHealth;
        }
    }


    public virtual void ShowGameOver(string winner)
    {
        if (GameOverWindow != null)
        {
            GameOverWindow.SetActive(true);
            playerWinText.text = winner;
        }
    }
}
