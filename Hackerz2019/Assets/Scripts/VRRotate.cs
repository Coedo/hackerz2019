﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
public class VRRotate : MonoBehaviour
{
    private void Update()
    {
        if(SteamVR_Input._default.inActions.RotateLeft.GetStateDown(SteamVR_Input_Sources.RightHand))
        {
            transform.Rotate(Vector3.up, -45);
        }
        if (SteamVR_Input._default.inActions.RotateRight.GetStateDown(SteamVR_Input_Sources.RightHand))
        {
            transform.Rotate(Vector3.up, 45);
        }
    }
}
