﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BulletData {
    public Bullet bulletPrefab;
    public uint maxAmmoCount;
    public bool infinity;
    public Sprite sprite;

}

[CreateAssetMenu]
public class BulletsLevelList : ScriptableObject {
    public BulletData[] bulletData;

}