﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum TrebuchetState {
    Ready,
    Firing,
    Idle,
    Reloading,
}

public class Trebuchet : MonoBehaviour {

    public event System.Action<Bullet> OnFired;
    public float _Power;
    public float _MaxPower;
    public BulletsLevelList _BulletPrefabs;
    public Transform _BulletSpawnPoint;

    public Transform _Arm;
    public Vector2 _ArmSwingMinMax;

    [Header("Animation")]
    public float _SwingTime;
    public float _PostFireSwingTime;
    public float _ReloadTime;

    public AnimationCurve _PreFireAnimation;
    public AnimationCurve _PostFireAnimation;
    public AnimationCurve _ReloadAnimation;

    public Vector2 _RotationMinMax;

    public TrebuchetState _state;

    Animator _Anim;

    float _StartRotation;

    public int _CurrentBulletIndex;

    Bullet _spawnedBullet;
    BulletData _selectedBulletData;

    uint[] _localAmunition;

    private void Awake() {
        _localAmunition = new uint[_BulletPrefabs.bulletData.Length];
        for (int i = 0; i < _BulletPrefabs.bulletData.Length; i++) {
            _localAmunition[i] = _BulletPrefabs.bulletData[i].maxAmmoCount;
        }
        _StartRotation = _ArmSwingMinMax.x;

    }

    private void Start() {
        SetPower(_Power);

        SetBulletIndex(0);
    }

    public void RotateAngle(float delta) {
        float rotation = transform.rotation.eulerAngles.y;
        rotation += delta;

        rotation = Mathf.Clamp(rotation, _RotationMinMax.x, _RotationMinMax.y);

        transform.rotation = Quaternion.Euler(Vector3.up * rotation);
    }

    public void SetRotationPercentage(float percent) {
        var value = Mathf.Lerp(_RotationMinMax.x, _RotationMinMax.y, percent);
        transform.rotation = Quaternion.Euler(Vector3.up * value);
    }

    public void Fire() {
        if (_state == TrebuchetState.Ready && _spawnedBullet != null) {
            StartCoroutine(FireCourutine());
        }
    }

    IEnumerator FireCourutine() {
        _state = TrebuchetState.Firing;

        var swingDelta = 1 / _SwingTime;
        float percent = 0f;

        var firedBullet = _spawnedBullet;
        _spawnedBullet = null;

        while (percent < 1) {
            float eval = _PreFireAnimation.Evaluate(percent);
            float l = Mathf.Lerp(_ArmSwingMinMax.x, _ArmSwingMinMax.y, eval);

            _Arm.localRotation = Quaternion.Euler(Vector3.forward * l);

            percent += swingDelta * Time.deltaTime;
            yield return null;
        }

        //Fire Bullet
        //var bullet = Instantiate(_BulletPrefabs[_CurrentBulletIndex], _BulletSpawnPoint.position, _BulletSpawnPoint.rotation);
        firedBullet.transform.parent = null;
        firedBullet.SetVelocity(_Power);
        _localAmunition[_CurrentBulletIndex]--;

        OnFired?.Invoke(firedBullet);

        percent = 0f;
        swingDelta = 1 / _PostFireSwingTime;
        while (percent < 1) {
            float eval = _PostFireAnimation.Evaluate(percent);
            float l = Mathf.Lerp(_ArmSwingMinMax.x, _ArmSwingMinMax.y, eval);

            _Arm.localRotation = Quaternion.Euler(Vector3.forward * l);

            percent += swingDelta * Time.deltaTime;
            yield return null;
        }

        _state = TrebuchetState.Idle;
        yield return RealoadCoroutine();
    }

    public void Reload() {
        if (_state == TrebuchetState.Idle)
            StartCoroutine(RealoadCoroutine());
    }

    IEnumerator RealoadCoroutine() {
        var swingDelta = 1 / _ReloadTime;
        float percent = 0f;

        _state = TrebuchetState.Reloading;

        while (percent < 1) {
            float eval = _ReloadAnimation.Evaluate(percent);
            float l = Mathf.Lerp(_ArmSwingMinMax.x, _ArmSwingMinMax.y, eval);

            _Arm.localRotation = Quaternion.Euler(Vector3.forward * l);

            percent += swingDelta * Time.deltaTime;
            yield return null;
        }

        if (_localAmunition[_CurrentBulletIndex] > 0 || _selectedBulletData.infinity) {
            SetBulletIndex(_CurrentBulletIndex);
        } else {
            SetBulletIndex(0);

        }
        _state = TrebuchetState.Ready;
    }

    public void SetPower(float value) {
        _Power = value;
        _Power = Mathf.Clamp(_Power, 0, _MaxPower);

        _ArmSwingMinMax.x = _StartRotation - _Power / 10f;
        _Arm.localRotation = Quaternion.Euler(Vector3.forward * _ArmSwingMinMax.x);
    }

    public void SetPowerPercentage(float percent) {
        var value = Mathf.Lerp(0, _MaxPower, percent);
        SetPower(value);
    }

    public void SetBulletIndex(int index) {
        var trySelect = _BulletPrefabs.bulletData[index];
        if (_localAmunition[index] > 0 || trySelect.infinity) {
            _CurrentBulletIndex = index;

            if (_spawnedBullet != null) {
                Destroy(_spawnedBullet.gameObject);
            }

            _selectedBulletData = trySelect;

            _spawnedBullet = Instantiate(_selectedBulletData.bulletPrefab, _BulletSpawnPoint.position, _BulletSpawnPoint.rotation);
            _spawnedBullet.LockPosition();

            _spawnedBullet.transform.parent = _BulletSpawnPoint;
        }
    }

    public uint GetAmmo(int index) {
        return _localAmunition[index];
    }

}