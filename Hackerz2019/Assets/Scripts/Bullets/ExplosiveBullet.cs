﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosiveBullet : Bullet {
    public float _ExplosionRadius;
    public float _ExplosionForce;
    public ParticleSystem _ExplosionParticle;

    bool _destroyed;

    private void OnCollisionEnter(Collision other) {
        if (_destroyed)
            return;

        var colliders = Physics.OverlapSphere(transform.position, _ExplosionRadius);
        foreach (var item in colliders) {
            var body = item.GetComponent<Rigidbody>();
            if (body) {
                body.AddExplosionForce(_ExplosionForce, transform.position, _ExplosionRadius, 1, ForceMode.Impulse);
            }
        }

        var particle = Instantiate(_ExplosionParticle, transform.position, Quaternion.identity);
        Destroy(particle.gameObject, particle.main.duration);
        Destroy(gameObject);

        _destroyed = true;
    }
}