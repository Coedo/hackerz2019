﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotgunBullet : Bullet {
    public Bullet _SmallBulletPrefab;
    public uint _SmallBulletCount;
    public float _TimeToExplosion;

    List<Bullet> _smallBullets = new List<Bullet>();

    protected override void Awake() {
        base.Awake();
        SpawnBullets();
    }

    public override void SetVelocity(float value) {
        base.SetVelocity(value);
        Invoke("Release", _TimeToExplosion);
        //SpawnBullets();
    }

    void SpawnBullets() {
        var size = GetComponent<SphereCollider>().radius;
        for (int i = 0; i < _SmallBulletCount; i++) {
            var posision = Random.insideUnitSphere * size + transform.position;

            var bullet = Instantiate(_SmallBulletPrefab, posision, transform.rotation, transform);
            //bullet.SetVelocity(_body.velocity.magnitude);
            bullet.LockPosition();
            _smallBullets.Add(bullet);
        }
    }

    void Release() {
        for (int i = 0; i < _smallBullets.Count; i++) {
            _smallBullets[i].transform.parent = null;
            //_smallBullets[i].SetVelocity(_body.velocity.magnitude);

            var vel = _body.velocity;
            var randomVec = Random.insideUnitCircle;

            vel += new Vector3(randomVec.x, randomVec.y, 0);

            _smallBullets[i].SetVelocityVector(vel);
        }

        Destroy(gameObject);
    }

    public override Transform GetTrackingTransform() {
        return transform.GetChild(0);
    }

}