﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Bullet : MonoBehaviour {

    protected Rigidbody _body;

    protected virtual void Awake() {
        _body = GetComponent<Rigidbody>();
    }

    public virtual void SetVelocity(float value) {
        _body.isKinematic = false;
        var coll = GetComponent<Collider>();
        if (coll) {
            coll.enabled = true;
        }

        _body.velocity = transform.up * value;
    }

    public virtual void LockPosition() {
        _body.isKinematic = true;
        GetComponent<Collider>().enabled = false;
    }

    public virtual Transform GetTrackingTransform() {
        return transform;
    }

    public virtual void SetVelocityVector(Vector3 vector3) {
        _body.isKinematic = false;
        var coll = GetComponent<Collider>();
        if (coll) {
            coll.enabled = true;
        }
        _body.velocity = vector3;
    }
}