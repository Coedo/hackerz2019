﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainBullet : Bullet {

    public Bullet[] _ChainEnds;

    public override void SetVelocity(float value) {
        foreach (var item in _ChainEnds) {
            item.SetVelocityVector(transform.up * value * 1.3f);
        }
    }

    public override void LockPosition() {
        foreach (var item in _ChainEnds) {
            item.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public override Transform GetTrackingTransform() {
        return _ChainEnds[0].transform;
    }
}