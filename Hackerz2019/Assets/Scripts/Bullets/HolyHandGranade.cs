﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HolyHandGranade : Bullet {
    public float _ExplosionForce;
    public float _ExplosionRadius;
    public ParticleSystem _ExplosionParticle;

    bool _destroyed;

    private void OnCollisionEnter(Collision other) {
        Invoke("Boom", 3f);
    }

    void Boom() {
        if (_destroyed)
            return;

        Debug.Log("I Święty Attyla wzniósł granat ręczny ku górze, mówiąc: O Panie, błogosław ten granat ręczny, który rozniesie nieprzyjaciół twych na malutkie kawałeczki w łasce twojej. A Pan skrzywił się w uśmiechu, a ludzie spożywać poczęli owce, leniwce, karpie, orangutany, płatki śniadaniowe, przetwory owocowe... I Pan powiedział: Wpierw wyjąć musisz świętą Zawleczkę, potem masz zliczyć do trzech, nie mniej, nie więcej. Trzy ma być liczbą, do której liczyć masz i liczbą tą ma być trzy. Do czterech nie wolno ci liczyć, ani do dwóch. Masz tylko policzyć do trzech. Pięć jest wykluczone. Gdy liczba trzy jako trzecia w kolejności osiągnięta zostanie, wówczas rzucić masz Święty Granat Ręczny z Antiochii w kierunku wroga, co naigrawał się z ciebie w polu widzenia twego, a wówczas on kitę odwali. Amen.!");
        var colliders = Physics.OverlapSphere(transform.position, _ExplosionRadius);
        foreach (var item in colliders) {
            var body = item.GetComponent<Rigidbody>();
            if (body) {
                body.AddExplosionForce(_ExplosionForce, transform.position, _ExplosionRadius, 1, ForceMode.Impulse);
            }
        }

        var particle = Instantiate(_ExplosionParticle, transform.position, Quaternion.identity);
        Destroy(particle.gameObject, particle.main.duration);
        _destroyed = true;

        Destroy(gameObject);
    }
}