﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TribleBullet : Bullet {
    public Bullet[] _Children;

    public override void SetVelocity(float velocity) {
        _body.isKinematic = false;
        foreach (var item in _Children) {
            item.transform.parent = null;
            item.SetVelocity(velocity);
        }

        Destroy(gameObject);
    }

    public override void LockPosition() {
        _body.isKinematic = true;
        foreach (var item in _Children) {
            item.GetComponent<Rigidbody>().isKinematic = true;
        }
    }

    public override Transform GetTrackingTransform() {
        return _Children[0].transform;
    }
}