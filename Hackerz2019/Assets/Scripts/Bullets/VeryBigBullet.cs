﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VeryBigBullet : Bullet {

    public float _StartScale = 1;
    public float _MaxScale;
    public float _TimeToMaxScale;

    float _growthSpeed;
    float _growthPercent;

    bool _fired;

    private void Start() {
        _growthSpeed = 1 / _TimeToMaxScale;
    }

    public override void SetVelocity(float value) {
        base.SetVelocity(value);
        _fired = true;
    }

    void Update() {
        if (_fired) {
            _growthPercent += _growthSpeed * Time.deltaTime;

            transform.localScale = Vector3.one * Mathf.Lerp(_StartScale, _MaxScale, _growthPercent);
        }
    }
}