﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MAinMenuController : MonoBehaviour {

    public string _MainSceneName;
    public Bullet[] _MenuBullets;
    public Transform _Target;

    private void Start() {
        StartCoroutine(FireBullets());        
    }

    public void StartGame() {
        SceneManager.LoadScene(_MainSceneName);
    }

    public void ExitGame() {
        Application.Quit();
    }

    IEnumerator FireBullets() {

        var dir = _Target.position - transform.position;

        for (int i = 0; i < _MenuBullets.Length; i++)
        {
            yield return new WaitForSeconds(1f);

            _MenuBullets[i].SetVelocityVector(dir.normalized * 60f);
        }
    }
}