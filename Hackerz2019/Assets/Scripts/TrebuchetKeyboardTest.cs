﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrebuchetKeyboardTest : MonoBehaviour {

    public Trebuchet _Trebuchet;

    void Update() {
        // if (Input.GetKeyDown(KeyCode.Space)) {
        //    _Trebuchet.Fire();
        // }

        if (Input.GetKey(KeyCode.DownArrow)) {
            _Trebuchet.SetPower(_Trebuchet._Power - .5f);
        }

        if (Input.GetKey(KeyCode.UpArrow)) {
            _Trebuchet.SetPower(_Trebuchet._Power + .5f);
        }

        if (Input.GetKeyDown(KeyCode.Alpha1)) {
            _Trebuchet.SetBulletIndex(0);
        }
        if (Input.GetKeyDown(KeyCode.Alpha2)) {
            _Trebuchet.SetBulletIndex(1);
        }
        if (Input.GetKeyDown(KeyCode.Alpha3)) {
            _Trebuchet.SetBulletIndex(2);
        }
        if (Input.GetKeyDown(KeyCode.Alpha4)) {
            _Trebuchet.SetBulletIndex(3);
        }

        if (Input.GetKeyDown(KeyCode.Alpha5)) {
            _Trebuchet.SetBulletIndex(4);
        }

        if (Input.GetKeyDown(KeyCode.Alpha6)) {
            _Trebuchet.SetBulletIndex(5);
        }

        if (Input.GetKey(KeyCode.RightArrow)) {
            _Trebuchet.RotateAngle(5f * Time.deltaTime);
        }

        if (Input.GetKey(KeyCode.LeftArrow)) {
            _Trebuchet.RotateAngle(-5f * Time.deltaTime);

        }
    }
}