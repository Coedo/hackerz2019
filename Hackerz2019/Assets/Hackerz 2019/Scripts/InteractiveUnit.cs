﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveUnit : MonoBehaviour
{

    public event Action<InteractiveUnit> OnUnitDie;

    public float currentHealth;

    public int maxHealth;

    public float baseInpactForce = 5;

    internal void Initialize()
    {
        currentHealth = maxHealth;
    }

    private void OnCollisionEnter(Collision collision)
    {       
        //Debug.Log("Relative Magnitude: " + collision.relativeVelocity.sqrMagnitude + " BaseInpactForce: " + baseInpactForce);
        if(collision.relativeVelocity.sqrMagnitude > baseInpactForce)
        {
            currentHealth -= collision.relativeVelocity.sqrMagnitude - baseInpactForce;
            if(currentHealth < 0)
            {
                UnitDie();
            }
        }
    }

    private void UnitDie()
    {
        if (OnUnitDie != null)
        {
            OnUnitDie(this);
        }
        Destroy(gameObject);
    }

    internal void DeactivateUnit()
    {
        MeshRenderer[] meshRenderers = GetComponentsInChildren<MeshRenderer>();
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].material.color = Color.red;
        }
    }
}
