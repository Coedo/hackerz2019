﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager instance;
    public static GameManager Instance
    {
        get
        {
            if(instance == null)
            {
                instance = FindObjectOfType<GameManager>();
                if(instance == null)
                {
                    Debug.LogError("Missing GameManager on scene.");
                }
            }
            return instance;
        }
    }

    public event Action OnGameStart;
    public event Action<PlayerController> OnPrepareUnitsStart;
    public event Action OnPrepareUnitsEnd;
    public event Action OnFightStart;
    public event Action OnFightEnd;
    public event Action<string> OnGameEnd;

    public List<PlayerController> playerControllers;

    public TurnSystem turnSystem;

    public bool isGameStarted;
    public bool isFightStarted;

    public bool oneByOne = true;
    public bool setUnitsInSameTime = false;
    private void Awake()
    {
        Initialize();
    }

    private void Initialize()
    {
        for (int i = 0; i < playerControllers.Count; i++)
        {
            playerControllers[i].Initialize();
            playerControllers[i].OnPlayerSetUnit += GameManager_OnPlayerSetUnit;
            playerControllers[i].OnPlayerReadyToFight += GameManager_OnPlayerReadyToFight;
            playerControllers[i].playerCastle.OnCastleLost += PlayerCastle_OnCastleLost;
            playerControllers[i].OnPlayerReadyToFire += GameManager_OnPlayerReadyToFire;
        }

        isGameStarted = true;
        if (OnGameStart != null)
        {
            OnGameStart();
        }

        if (!setUnitsInSameTime)
        {
            if (OnPrepareUnitsStart != null)
            {
                OnPrepareUnitsStart(playerControllers[0]);
            }
        }
        else
        {
            for (int i = 0; i < playerControllers.Count; i++)
            {
                OnPrepareUnitsStart(playerControllers[i]);
            }
        }
    }

    private void GameManager_OnPlayerReadyToFire(PlayerController playerController)
    {
        Debug.Log("GameManager_OnPlayerReadyToFire");
        if (oneByOne)
        {
            turnSystem.ExecuteTurn(playerController);
        }
        else
        {
            
            bool allReady = true;
            for (int i = 0; i < playerControllers.Count; i++)
            {
                allReady &= playerControllers[i].readyToFight;
            }
            if (allReady)
            {
                turnSystem.ExecuteTurn();                
                StartFight();
            }
        }
    }

    private void PlayerCastle_OnCastleLost(CastleManager castleManager)
    {
        if (isFightStarted && isGameStarted)
        {
            Debug.Log("PlayerCastle_OnCastleLost: " + castleManager.name);
            for (int i = 0; i < playerControllers.Count; i++)
            {
                if (playerControllers[i].playerCastle == castleManager)
                {
                    isFightStarted = false;
                    if (OnFightEnd != null)
                    {
                        OnFightEnd();
                    }
                    isGameStarted = false;
                    if (OnGameEnd != null)
                    {
                        string winnerText = "";
                        if (i == 0)
                        {
                            winnerText = "Player " + 2 + "won!";
                        }
                        else
                        {
                            winnerText = "Player " + 1 + "won!";
                        }

                        OnGameEnd(winnerText);
                    }
                }
            }
        }
    }

    private void GameManager_OnPlayerReadyToFight(PlayerController playerController)
    {
        Debug.Log("GameManager_OnPlayerReadyToFight");
        bool allReady = true;

        for (int i = 0; i < playerControllers.Count; i++)
        {
            allReady &= playerControllers[i].readyToFight;
        }
        if(allReady)
        {
            if(OnPrepareUnitsEnd != null)
            {
                OnPrepareUnitsEnd();
            }
            StartFight();
        }
        else
        {
            if(oneByOne && !setUnitsInSameTime)
            {
                for (int i = 0; i < playerControllers.Count; i++)
                {
                    if(!playerControllers[i].readyToFight)
                    {
                        OnPrepareUnitsStart(playerControllers[i]);
                    }
                }
            }
        }
    }

    private void StartFight()
    {
        Debug.Log("Start Fight");
        isFightStarted = true;
        if (OnFightStart != null)
        {
            OnFightStart();
        }
        turnSystem.StartTurn();
    }

    private void GameManager_OnPlayerSetUnit(PlayerController playerController, VillageUnit villageUnit)
    {
        Debug.Log("GameManager_OnPlayerSetUnit");
    }
}
