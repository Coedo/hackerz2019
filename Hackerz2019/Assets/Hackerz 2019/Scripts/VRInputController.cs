﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
public class VRInputController : GameInputController
{

    public Transform controllerRaycast;

    public TrebuchetKeyboardTest trebuchetKeyboard;
    public LineRenderer lineRenderer;
    public UIController UIController;

    public override void Initialize(PlayerController playerController)
    {
        trebuchetKeyboard.enabled = false;
        base.Initialize(playerController);
        playerController.playerCastle.OnCastleLostUnit += PlayerCastle_OnCastleLostUnit;
    }

    private void PlayerCastle_OnCastleLostUnit(CastleManager castleManager)
    {
        UIController.unitsAlive.text = castleManager.villagers.Count.ToString();
    }

    public override Vector3 GetBlockPosition(CastleBlock castleBlock)
    {
        if (castleBlock != null && castleBlock.canSetUnit)
        {
            if (playerController.playerCastle.castleBlocks.Contains(castleBlock))
            {
                return castleBlock.GetTopPosition();
            }
        }

        return Vector3.zero;
    }

    protected override IEnumerator SetUnitsCoroutine()
    {
        while (true)
        {
            yield return null;
            RaycastHit hit;
            InteractiveUnit interactiveUnit = GetHitInteractiveUnit(out hit);

            DrawRaycastLine(hit, interactiveUnit);

            if (SteamVR_Input._default.inActions.InteractUI.GetStateDown(SteamVR_Input_Sources.RightHand))
            {            
                if (interactiveUnit != null)
                {
                    if (selectedUnit != null)
                    {
                        if (interactiveUnit is CastleBlock)
                        {
                            Vector3 unitPositon = GetBlockPosition(interactiveUnit as CastleBlock);
                            if (unitPositon != Vector3.zero)
                            {
                                playerController.SetUnit(selectedUnit, unitPositon);
                                selectedUnit = playerController.GetNextFreeUnit();
                            }
                        }
                    }
                    if (interactiveUnit is VillageUnit)
                    {
                        VillageUnit unit = interactiveUnit as VillageUnit;
                        if (playerController.playerCastle.villagers.Contains(unit) && !playerController.villagersToSet.Contains(unit))
                        {
                            Debug.Log("Reset unit");
                            playerController.AddToSetUnits(unit);
                            selectedUnit = unit;
                        }
                    }
                    UIController.UpdateUnitsToSet(playerController.LeftUnitsToSet());
                }


            }
            if (SteamVR_Input._default.inActions.GrabGrip.GetStateDown(SteamVR_Input_Sources.RightHand))
            {
                playerController.FinishPreparingUnits();
            }
        }
    }



    private void DrawRaycastLine(RaycastHit hit, InteractiveUnit interactiveUnit)
    {
        if (hit.collider != null)
        {
            lineRenderer.enabled = true;
            lineRenderer.SetPositions(new Vector3[] { controllerRaycast.position, hit.point });
            if (interactiveUnit != null)
            {
                lineRenderer.endColor = Color.green;
            }
            else
            {
                lineRenderer.endColor = Color.red;
            }
        }
        else
        {
            lineRenderer.endColor = Color.red;
            lineRenderer.enabled = false;
            lineRenderer.SetPositions(new Vector3[] { });
        }
    }

    private InteractiveUnit GetHitInteractiveUnit(out RaycastHit rayhit)
    {
       
        RaycastHit hit;

        if (Physics.Raycast( controllerRaycast.position, controllerRaycast.forward, out hit, 100))
        {
            rayhit = hit;
            InteractiveUnit interactiveUnit = hit.collider.GetComponentInParent<InteractiveUnit>();
            if (interactiveUnit != null)
            {
                return interactiveUnit;
            }
        }
        rayhit = hit;
        return null;
    }

    public override void StopSettingUnits()
    {
        RaycastHit hit = new RaycastHit();
        DrawRaycastLine(hit, null);
        UIController.HideUnitsInfo();
        Debug.Log("StopSettingUnits");
        if (GameManager.Instance.setUnitsInSameTime)
        {
            UIController.ShowWaitingForSecondPlayer();
        }
        base.StopSettingUnits();
    }

    public override void StartFightTurn()
    {
        Debug.Log("StartFightTurn: " + gameObject.name);
        StopAllCoroutines();
        UIController.HideWaitingForSecondPlayer();
        UIController.ShowPlayersTurn();       
        UIController.SetTrebuchet(trebuchetKeyboard._Trebuchet);
        UIController.OnFireClick += MouseUIController_OnFireClick;
        StartCoroutine(FightCoroutine());
        base.StartFightTurn();
    }

    private void MouseUIController_OnFireClick()
    {
        playerController.FinishFightTurn();
    }

    protected override IEnumerator FightCoroutine()
    {
        trebuchetKeyboard.enabled = true;
        Debug.Log("FightCoroutine: " + gameObject.name);
        while (true)
        {
            yield return null;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("FightCoroutine + GetKeyDown: " + gameObject.name);
                playerController.FinishFightTurn();
            }
        }
    }

    public override void StopFightTurn()
    {
        Debug.Log("StopFightTurn: " + gameObject.name);
        UIController.OnFireClick -= MouseUIController_OnFireClick;
        StopAllCoroutines();
        trebuchetKeyboard.enabled = false;
        base.StopFightTurn();
    }
}
