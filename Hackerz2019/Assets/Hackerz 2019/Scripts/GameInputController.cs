﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInputController : MonoBehaviour, IInputController
{

    protected PlayerController playerController;

    public VillageUnit selectedUnit;

    public virtual void Initialize(PlayerController playerController)
    {
        this.playerController = playerController;
        GameManager.Instance.OnFightEnd += Instance_OnFightEnd;
    }

    private void Instance_OnFightEnd()
    {
        StopAllCoroutines();
    }

    public void StartSettingUnits()
    {
        
        SelectVillager(playerController.villagersToSet[0]);
        Debug.LogError(selectedUnit);
        StartCoroutine(SetUnitsCoroutine());
    }

    private void SelectVillager(VillageUnit villageUnit)
    {
        selectedUnit = villageUnit;
    }

    protected virtual IEnumerator SetUnitsCoroutine()
    {
        yield return null;
    }

    public virtual void StopSettingUnits()
    {
        StopAllCoroutines();
    }

    public virtual Vector3 GetBlockPosition(CastleBlock castleBlock)
    {
        return Vector3.negativeInfinity;
    }

    public virtual void StartFightTurn()
    {

    }

    public virtual void StopFightTurn()
    {

    }

    protected virtual IEnumerator FightCoroutine()
    {
        yield return null;
    }
}
