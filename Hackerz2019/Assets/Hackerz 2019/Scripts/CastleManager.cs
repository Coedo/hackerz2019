﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleManager : MonoBehaviour
{

    public event Action<CastleManager> OnCastleLost;
    public event Action<CastleManager> OnCastleLostUnit;

    public List<CastleBlock> castleBlocks;

    public List<VillageUnit> villagers;

    public float castleBlocksLimitPercentage = 20;
    public int CurrentHealth
    {
        get
        {
            return castleBlocks.Count;
        }
    }
    public int MaxHealth;
    public void Initialize()
    {

        castleBlocks.AddRange(GetComponentsInChildren<CastleBlock>());
        villagers.AddRange(GetComponentsInChildren<VillageUnit>());
        MaxHealth = castleBlocks.Count;
        for (int i = 0; i < castleBlocks.Count; i++)
        {
            castleBlocks[i].Initialize();
            castleBlocks[i].OnUnitDie += CastleManager_OnBlockUnitDie;
        }
        for (int i = 0; i < villagers.Count; i++)
        {
            villagers[i].Initialize();
            villagers[i].OnUnitDie += CastleManager_OnVillageUnitDie;
        }
    }

    private void CastleManager_OnVillageUnitDie(InteractiveUnit interactiveUnit)
    {       
        RemoveCastleVillager(interactiveUnit);        
    }

    private bool RemoveCastleVillager(InteractiveUnit interactiveUnit)
    {
        if (interactiveUnit is VillageUnit && villagers.Contains((VillageUnit)interactiveUnit))
        {
            Debug.LogWarning("Removing castle Villager");
            villagers.Remove((VillageUnit)interactiveUnit);
            interactiveUnit.OnUnitDie -= CastleManager_OnVillageUnitDie;
            if (OnCastleLostUnit != null)
            {
                OnCastleLostUnit(this);
            }
            ValidateCastle();
            return true;
        }
        return false;
    }

    private void CastleManager_OnBlockUnitDie(InteractiveUnit interactiveUnit)
    {
        RemoveCastleBlock(interactiveUnit);       
    }

    private bool RemoveCastleBlock(InteractiveUnit interactiveUnit)
    {
        if (interactiveUnit is CastleBlock && castleBlocks.Contains((CastleBlock)interactiveUnit))
        {
            Debug.LogWarning("Removing castle Block");
            castleBlocks.Remove((CastleBlock)interactiveUnit);
            interactiveUnit.OnUnitDie -= CastleManager_OnBlockUnitDie;
            if (OnCastleLostUnit != null)
            {
                OnCastleLostUnit(this);
            }
            ValidateCastle();
            return true;
        }
        return false;
    }

    private void ValidateCastle()
    {
        if (((float)CurrentHealth / (float)MaxHealth) < castleBlocksLimitPercentage * 0.01f || villagers.Count == 0)
        {
            if (OnCastleLost != null)
            {
                OnCastleLost(this);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (GameManager.Instance.isFightStarted)
        {
            InteractiveUnit interactiveUnit = other.GetComponentInParent<InteractiveUnit>();
            if (interactiveUnit != null)
            {
                if (interactiveUnit is CastleBlock)
                {
                    Debug.LogError("CastleBlock LEFT AREA!");
                    if (RemoveCastleBlock(interactiveUnit))
                    {
                        interactiveUnit.DeactivateUnit();
                    }
                }
                else if (interactiveUnit is VillageUnit)
                {
                    Debug.LogError("VillageUnit LEFT AREA!");
                    if (RemoveCastleVillager(interactiveUnit))
                    {
                        interactiveUnit.DeactivateUnit();
                    }
                }
                ValidateCastle();
            }
        }
    }
}
