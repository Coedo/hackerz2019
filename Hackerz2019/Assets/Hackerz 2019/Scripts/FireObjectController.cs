﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireObjectController : MonoBehaviour
{
    public Camera povCamera;
    public Rigidbody fireObjectPrefab;

    public float force = 10;

    private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Rigidbody newObject = Instantiate(fireObjectPrefab);
            newObject.transform.position = povCamera.transform.position;
            newObject.AddForce(povCamera.transform.forward * force, ForceMode.VelocityChange);
            Destroy(newObject.gameObject, 10f);
        }
    }

}
