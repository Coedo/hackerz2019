﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CastleBlock : InteractiveUnit
{
    public bool canSetUnit;

    public Vector3 GetTopPosition()
    {
        Vector3 position = transform.position;
        position.y += transform.GetComponentInChildren<Collider>().bounds.size.y;
        return position;
    }
}
