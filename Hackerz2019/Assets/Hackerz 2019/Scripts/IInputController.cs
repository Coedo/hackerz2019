﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInputController
{

    void StartSettingUnits();
    void StopSettingUnits();
    Vector3 GetBlockPosition(CastleBlock castleBlock);
}
