﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnSystem : MonoBehaviour
{    

    public event Action<PlayerController> OnStartTurn;
    public event Action<PlayerController> OnExecuteTurn;
    public event Action<PlayerController> OnEndTurn;
    public event Action<PlayerController> OnNextTurn;

    public int turnIndex;
    public int playerIndex;

    public void StartTurn()
    {
        if(!GameManager.Instance.isGameStarted || !GameManager.Instance.isFightStarted)
        {
            return;
        }

            turnIndex++;
        if(GameManager.Instance.oneByOne)
        {
            playerIndex++;
            playerIndex = playerIndex % GameManager.Instance.playerControllers.Count;

            Debug.Log("StartTurn: " + GameManager.Instance.playerControllers[playerIndex].name);
            if (OnStartTurn != null)
            {
                OnStartTurn(GameManager.Instance.playerControllers[playerIndex]);
            }

        }
        else
        {
            for (int i = 0; i < GameManager.Instance.playerControllers.Count; i++)
            {
                Debug.Log("ExecuteTurn: " + GameManager.Instance.playerControllers[playerIndex].name);
                if (OnStartTurn != null)
                {
                    OnStartTurn(GameManager.Instance.playerControllers[i]);
                }
            }
        }

    }

    internal void ExecuteTurn(PlayerController playerController)
    {
        StopAllCoroutines();
        if (!GameManager.Instance.isGameStarted || !GameManager.Instance.isFightStarted)
        {
            return;
        }
        if (OnExecuteTurn != null)
        {
            OnExecuteTurn(playerController);
        }
        Debug.Log("ExecuteTurn: " + playerController.name);
        StartCoroutine(EndTurn(playerController));

    }

    private IEnumerator EndTurn(PlayerController playerController)
    {
        yield return new WaitForSeconds(10f);
        Debug.Log("EndTurn: " + playerController.name);
        if (!GameManager.Instance.isGameStarted || !GameManager.Instance.isFightStarted)
        {
            yield break;
        }
        if (OnEndTurn != null)
        {
            OnEndTurn(playerController);
        }
        yield return new WaitForSeconds(1f);
        if (!GameManager.Instance.isGameStarted || !GameManager.Instance.isFightStarted)
        {
            yield break;
        }
        StartTurn();
    }

    internal void ExecuteTurn()
    {
        StopAllCoroutines();
        if (!GameManager.Instance.isGameStarted || !GameManager.Instance.isFightStarted)
        {
            return;
        }
        for (int i = 0; i < GameManager.Instance.playerControllers.Count; i++)
        {
            if (OnExecuteTurn != null)
            {
                OnExecuteTurn(GameManager.Instance.playerControllers[i]);
            }
        }
    }
}
