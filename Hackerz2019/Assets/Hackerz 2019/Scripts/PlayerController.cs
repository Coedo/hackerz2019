﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public CastleManager playerCastle;

    public event Action<PlayerController, VillageUnit> OnPlayerSetUnit;
    public event Action<PlayerController> OnPlayerReadyToFight;
    public event Action<PlayerController> OnPlayerReadyToFire;

    public List<VillageUnit> villagersToSet;

    public Trebuchet trebuchet;

    public bool readyToFight;
    public bool readyToFire;
    public GameInputController GameInputController;

    public void Initialize()
    {
        if (playerCastle != null)
        {
            playerCastle.Initialize();

        }
        if(GameInputController != null)
        {
            GameInputController.Initialize(this);
        }
        GameManager.Instance.OnPrepareUnitsStart += Instance_OnPrepareUnitsStart;
        //GameManager.Instance.OnFightStart += Instance_OnFightStart;
        GameManager.Instance.turnSystem.OnStartTurn += TurnSystem_OnStartTurn;
        GameManager.Instance.turnSystem.OnExecuteTurn += TurnSystem_OnExecuteTurn;
        GameManager.Instance.turnSystem.OnEndTurn += TurnSystem_OnEndTurn;
        GameManager.Instance.turnSystem.OnNextTurn += TurnSystem_OnNextTurn;
    }

    private void TurnSystem_OnStartTurn(PlayerController playerController)
    {
        if (playerController == this)
        {
            readyToFire = false;
            GameInputController.StartFightTurn();  
        }
    }
    private void TurnSystem_OnExecuteTurn(PlayerController playerController)
    {
        if (playerController == this)
        {
            GameInputController.StopFightTurn();
            trebuchet.Fire();
        }
    }

    private void TurnSystem_OnEndTurn(PlayerController playerController)
    {
        if (playerController == this)
        {
            GameInputController.StopFightTurn();
        }
    }

    private void TurnSystem_OnNextTurn(PlayerController playerController)
    {
        if (playerController == this)
        {
            GameInputController.StopFightTurn();
        }
    }
    private void Instance_OnFightEnd()
    {
        GameInputController.StopFightTurn();
    }

    private void Instance_OnPrepareUnitsStart(PlayerController playerController)
    {
        if (playerController == this)
        {
            Debug.Log("Instance_OnPrepareUnitsStart: " + playerController.name);
            GameInputController.StartSettingUnits();
        }
    }

    public int LeftUnitsToSet()
    {
        return villagersToSet.Count;
    }

    public void SetUnit(VillageUnit selectedUnit, Vector3 unitPositon)
    {
        selectedUnit.transform.localRotation = Quaternion.identity;
        selectedUnit.transform.position = unitPositon;
        villagersToSet.Remove(selectedUnit);
        if(OnPlayerSetUnit != null)
        {
            OnPlayerSetUnit(this, selectedUnit);
        }
    }
    
    public VillageUnit GetNextFreeUnit()
    {
        if (villagersToSet.Count > 0)
        {
            return villagersToSet[0];
        }
        return null;
    }

    public void FinishPreparingUnits()
    {
        if (villagersToSet.Count == 0)
        {
            readyToFight = true;
            GameInputController.StopSettingUnits();
            if (OnPlayerReadyToFight != null)
            {
                OnPlayerReadyToFight(this);
            }
        }
    }

    public void AddToSetUnits(VillageUnit unit)
    {
        villagersToSet.Add(unit);        
    }

    public void FinishFightTurn()
    {
        if (trebuchet._state == TrebuchetState.Ready && !readyToFire)
        {
            readyToFire = true;
            if (OnPlayerReadyToFire != null)
            {
                OnPlayerReadyToFire(this);
            }
        }
    }
}
