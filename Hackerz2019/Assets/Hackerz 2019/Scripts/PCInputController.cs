﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCInputController : GameInputController
{
    public int baseDisplay = 0;
    public bool ChangeDisplay = true;
    public Camera playerPrepareUnitsCamera;
    public Camera playerFireCamera;

    public TrebuchetKeyboardTest trebuchetKeyboard;

    public UIController mouseUIController;

    bool prepareUnitCamera;

    public override void Initialize(PlayerController playerController)
    {
        trebuchetKeyboard.enabled = false;
        base.Initialize(playerController);
    }

    public override Vector3 GetBlockPosition(CastleBlock castleBlock)
    {
        if (castleBlock != null && castleBlock.canSetUnit)
        {
            if (playerController.playerCastle.castleBlocks.Contains(castleBlock))
            {
                return castleBlock.GetTopPosition();
            }
        }

        return Vector3.zero;
    }

    void Update() {
        if(Input.GetKeyDown(KeyCode.Tab)) {
            prepareUnitCamera = !prepareUnitCamera;

            if(prepareUnitCamera)
            {
                playerPrepareUnitsCamera.enabled = true;
                playerFireCamera.enabled = false;
            }
            else
            {
                                playerPrepareUnitsCamera.enabled = true;
                playerFireCamera.enabled = false;
            }
        }
    }

    protected override IEnumerator SetUnitsCoroutine()
    {
        playerPrepareUnitsCamera.targetDisplay = 0;
        while (true)
        {
            yield return null;
            if (Input.GetMouseButtonDown(0))
            {
                InteractiveUnit interactiveUnit = GetHitInteractiveUnit();

                if (interactiveUnit != null)
                {
                    if (selectedUnit != null)
                    {
                        if (interactiveUnit is CastleBlock)
                        {
                            Vector3 unitPositon = GetBlockPosition(interactiveUnit as CastleBlock);
                            if (unitPositon != Vector3.zero)
                            {
                                playerController.SetUnit(selectedUnit, unitPositon);
                                selectedUnit = playerController.GetNextFreeUnit();
                            }
                        }
                    }
                    if (interactiveUnit is VillageUnit)
                    {
                        VillageUnit unit = interactiveUnit as VillageUnit;
                        if (playerController.playerCastle.villagers.Contains(unit) && !playerController.villagersToSet.Contains(unit))
                        {
                            Debug.Log("Reset unit");
                            playerController.AddToSetUnits(unit);
                            selectedUnit = unit;
                        }
                    }
                    mouseUIController.UpdateUnitsToSet(playerController.LeftUnitsToSet());
                }
                

            }
            if(Input.GetKeyDown(KeyCode.Space))
            {
                playerController.FinishPreparingUnits();
            }
        }
    }

    private InteractiveUnit GetHitInteractiveUnit()
    {
        Ray ray = playerPrepareUnitsCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        Debug.DrawRay(ray.origin, ray.direction, Color.red, 5f);
        if (Physics.Raycast(ray, out hit, 100))
        {
            InteractiveUnit interactiveUnit = hit.collider.GetComponentInParent<InteractiveUnit>();
            if(interactiveUnit != null)
            {
                return interactiveUnit;
            }
        }
        return null;
    }

    public override void StopSettingUnits()
    {
        Debug.Log("StopSettingUnits");
        mouseUIController.HideUnitsInfo();
        if(GameManager.Instance.setUnitsInSameTime)
        {
            mouseUIController.ShowWaitingForSecondPlayer();
        }
        if (ChangeDisplay)
        {
            playerPrepareUnitsCamera.targetDisplay = 4;
        }
        base.StopSettingUnits();
    }

    public override void StartFightTurn()
    {
        Debug.Log("StartFightTurn: " + gameObject.name);
        StopAllCoroutines();       
        mouseUIController.HideWaitingForSecondPlayer();
        mouseUIController.SetTrebuchet(trebuchetKeyboard._Trebuchet);
        mouseUIController.OnFireClick += MouseUIController_OnFireClick;
        StartCoroutine(FightCoroutine());
        base.StartFightTurn();
    }

    private void MouseUIController_OnFireClick()
    {
        playerController.FinishFightTurn();
    }

    protected override IEnumerator FightCoroutine()
    {
        playerFireCamera.targetDisplay = baseDisplay;
        trebuchetKeyboard.enabled = true;
        Debug.Log("FightCoroutine: " + gameObject.name);
        while (true)
        {
            yield return null;

            if (Input.GetKeyDown(KeyCode.Space))
            {
                Debug.Log("FightCoroutine + GetKeyDown: " + gameObject.name);
                playerController.FinishFightTurn();
            }
        }
    }

    public override void StopFightTurn()
    {
        Debug.Log("StopFightTurn: " + gameObject.name);
        mouseUIController.OnFireClick -= MouseUIController_OnFireClick;
        if (ChangeDisplay)
        {
            playerFireCamera.targetDisplay = 3;
        }
        StopAllCoroutines(); 
        trebuchetKeyboard.enabled = false;       
        base.StopFightTurn();
    }
}
