﻿Shader "Custom/Damage"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _DamageTextture("Damage Texture", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _DmgPercent("Damage Percent", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;
        sampler2D _DamageTextture;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 worldNormal;
        };

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        float _DmgPercent;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            //fixed4 d = tex2D (_DamageTextture, IN.worldPos.xy);

            float3 blendNormal = saturate(pow(IN.worldNormal * 1.4,4));
 
            // normal noise triplanar for x, y, z sides
            float3 xn = tex2D(_DamageTextture, IN.worldPos.zy);
            float3 yn = tex2D(_DamageTextture, IN.worldPos.zx);
            float3 zn = tex2D(_DamageTextture, IN.worldPos.xy);

                    // lerped together all sides for noise texture
            float3 noisetexture = zn;
            noisetexture = lerp(noisetexture, xn, blendNormal.x);
            noisetexture = lerp(noisetexture, yn, blendNormal.y);



            o.Albedo = c.rgb - _DmgPercent * noisetexture;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
